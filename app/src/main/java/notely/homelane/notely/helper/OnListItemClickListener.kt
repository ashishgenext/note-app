package notely.homelane.notely.helper

import android.view.View

interface OnListItemClickListener {
    fun onItemClick(view : View?,position:Int)
}